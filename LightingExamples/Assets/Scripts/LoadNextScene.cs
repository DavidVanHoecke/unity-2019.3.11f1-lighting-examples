﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadNextScene : MonoBehaviour
{
    public string Scene;
    public int TimeToNextScene;

    void Start()
    {
        if (Scene == null)
            throw new InvalidOperationException("You must assign a value to the Scene property via the editor.");
    }

    // Update is called once per frame
    void Update()
    {
        if(Time.time > TimeToNextScene)
        {
            SceneManager.LoadScene(Scene);
        }
    }
}
